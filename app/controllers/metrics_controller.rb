class MetricsController < ApplicationController

  before_action :load_metric, except: :index
  before_action :load_user, only: [:edit, :new, :update]

  def index
    client = @hoopla_client
    metrics_url = client.get_relative_url("list_metrics")
    result = client.get(metrics_url, {'Accept': 'application/vnd.hoopla.metric-list+json'})

    @metrics = result.map { |m| Metric.new(m) }
  end

  def show
    users_url = @hoopla_client.get_relative_url("list_users")
    users = @hoopla_client.get(users_url, {'Accept': 'application/vnd.hoopla.users-list+json'})

    values = @hoopla_client.get(@metric.links['list_metric_values'], {'Accept': 'application/vnd.hoopla.metric-values-list+json'})

    @metric_values = []
    users.sort_by { |u| "#{u['first_name']} #{u['last_name']}" }.each do |user|
      value = values.select { |v| v['owner']['href'] == user['href'] }
      v =  0
      href = "" 

      if value.length > 0
        v = value[0]['value']
        href = value[0]['href']
      end

      @metric_values << MetricValue.new({href: href, user_href: user['href'], name: "#{user['first_name']} #{user['last_name']}", value: v})
    end

  end

  def edit
    value_href = CGI.unescape(params[:value_href])
    @metric_value = @hoopla_client.get(value_href, {'Accept': 'application/vnd.hoopla.metric-value+json'})

    @metric_updated_at = @metric_value['updated_at']
    @metric_value = @metric_value['value']

    @action = "Update"
    @form_url = update_metric_value_url(href: CGI.escape(@metric_href), value_href: CGI.escape(value_href))
    @form_method = 'put'
  end

  def new
    @action = "Create"

    @form_url = create_metric_value_url(CGI.escape(@metric.href))
    @form_method = 'post'
  end

  def create
    value = params[:v]

    payload = {
      owner: {
        kind: 'user',
        href: @user_href
      },
      value: value.to_f
    }

    res = @hoopla_client.post(@metric.links['create_metric_value'], payload, {'Accept': 'application/vnd.hoopla.metric-value+json'})

    redirect_to list_metric_values_url(CGI.escape(@metric_href))

  end

  def update
    value_href = CGI.unescape(params[:value_href])
    value = params[:v]

    @metric_value = @hoopla_client.get(value_href, {'Accept': 'application/vnd.hoopla.metric-value+json'})

    payload = {
      href: value_href,
      metric: {
        href: @metric_href
      },
      owner: {
        kind: 'user',
        href: @user_href
      },
      value: value.to_f,
      updated_at: @metric_value['updated_at']
    }

    res = @hoopla_client.put(value_href, payload, {'Accept': 'application/vnd.hoopla.metric-value+json'})

    redirect_to list_metric_values_url(CGI.escape(@metric_href))
  end


  private

  def load_metric
    @metric_href = CGI.unescape(params[:href])
    @metric = Metric.new(@hoopla_client.get(@metric_href, {'Accept': 'application/vnd.hoopla.metric+json'}))
  end

  def load_user
    @user_href = CGI.unescape(params[:user_href])
    @user = @hoopla_client.get(@user_href, {'Accept': 'application/vnd.hoopla.user+json'})
  end

end
