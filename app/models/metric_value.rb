class MetricValue
  include ActiveModel::Model
  attr_accessor :name,
    :value,
    :href,
    :user_href
end
