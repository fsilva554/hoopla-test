require 'json'

class Metric
  include ActiveModel::Model
  
  attr_accessor :href,
    :name,
    :type,
    :currency_code,
    :format_rounded_to,
    :links,
    :updated_at

  def initialize(values)
    self.links = {}
    values.each do |key, value|
      if 'links' == key
        value.each do |link|
          self.links[link['rel']] = link['href']
        end
      else
        send("#{key}=", value)
      end
    end
  end

  def persisted?
    false
  end
  
end
